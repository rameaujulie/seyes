# Seyes

Seyes affiche une page quadrillée de type cahier, et permet d'y ajouter du texte.

Pour l'utiliser rendez-vous sur https://educajou.forge.apps.education.fr/seyes/ 

## Licence

Seyes est une application libre sous licence GNU/GPL.

AA Cursive est une police cursive créée par A. Renaudin partagée sous licence OFL.

Open Dyslexic est une police d'écriture sous licence libre destinée à faciliter la lecture pour les personnes dyslexiques. Elle a été créée par Abelardo Gonzalez.

Belle Allure est une police d'écriture cursive créée par Jean Boyault. Si vous remixez l'application Seyes dans une nouvelle application, merci d'en informer l'auteur de Belle Allure pour lui demander l'autorisation de l'utiliser.


