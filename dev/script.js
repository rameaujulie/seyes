// Seyes est un logiciel libre
// sous licence GNU GPL
/// écrit par Arnaud Champollion


//Éléments DOM
const marge=document.getElementById('marge');
const page=document.getElementById('page');
const confirmationNouveau=document.getElementById('confirmation-nouveau');

const fullscreen=document.getElementById('fullscreen');
const bouton_a_propos=document.getElementById('a_propos');
const bouton_ligatures=document.getElementById('bouton_ligatures');
const bouton_copier=document.getElementById('copier');

const texte_principal=document.getElementById('texte_principal');
const body = document.body;
const site = document.documentElement;
const texte_apropos = document.getElementById('texte_apropos');
const boutons = document.getElementById('boutons');
const texte_marge = document.getElementById('texte_marge');
const choix_couleurs = document.getElementById('choix_couleurs');
const zone_menu_police = document.getElementById('zone_menu_police');
const zone_menu_copie_lien = document.getElementById('zone_menu_copie_lien');
const zone_lien = document.getElementById('zone_lien');
const boutons_police = document.getElementById('boutons_police');
const applique_couleur = document.getElementById('applique_couleur');
const voile_page = document.getElementById('voile_page');
const voile_marge = document.getElementById('voile_marge');


bouton_ligatures.style.display='none';

// Autres constantes
const isWindows = /Win/i.test(navigator.userAgent);
console.log("Sommes-nous sur Windows ? "+isWindows)

// Pour les ligatures
let signes=['═','║']
let subsitutions=[
    ['on','═'],
    ['or','║'],
    ['s ','╝ '],
    ['s\\.','╝.'],
    ['s,','╝,'],
    ['s;','╝;'],
    ['s:','╝:'],
    ['s!','╝!'],
    ['s/','╝/'],
    ['s-','╝-'],
    ["s'","╝'"],
    [' p',' ╰'],
    [' j',' ╮'],
    [' s',' ╠'],
    [' i',' ╱']
]

let polices=['AA Cursive','Open Dyslexic','Belle Allure GS','Belle Allure CE','Belle Allure CM']

// Variables globales
largeur_carreau=80;
facteur_taille_police=0.75
taille_police=60;
epaisseur_police='normal';
pleinecran_on=false;
a_propos_on=false;
redim=false;
ligatures_on=false;
dragged=null;
coef_marge=0.35;
coef_marge_windows=0.3125;
distance_soulignage=0.3;
focus_texte=null;
police_active=polices[0];
couleurSoulignementActive='rouge';

// Lecture des paramètres à lancer au départ
// Récupération du réglage
async function lire() {
    // Dans les paramètres de la page OpenBoard
    if (window.widget) { // Si OpenBoard
        // Quand on revient sur le widget, on récupère les paramètres stockés
        if (await window.sankore.async.preference('police')!='') {
            change_police(await window.sankore.async.preference('police'));
            texte_principal.innerHTML = await window.sankore.async.preference('texte_principal');
            texte_marge.innerHTML = await window.sankore.async.preference('texte_marge');
            applique_couleur.style.backgroundColor = await window.sankore.async.preference('applique_couleur');
            choix_couleurs.value = await window.sankore.async.preference('choix_couleurs');
        } else {
            change_police(police_active);
            // Couleur par défaut
            applique_couleur.style.backgroundColor='#0000FF'; // Pour le bouton principal
            choix_couleurs.value='#0000FF'; // Pour le sélecteur
        }
    }
    // dans la mémoire locale du navigateur
    else {

        if (localStorage.getItem('seyes-police')) {
            change_police(localStorage.getItem('seyes-police'))
            texte_principal.innerHTML = localStorage.getItem('seyes-texte_principal')
            texte_marge.innerHTML = localStorage.getItem('seyes-texte_marge')
            applique_couleur.style.backgroundColor = localStorage.getItem('seyes-applique_couleur')
            choix_couleurs.value = localStorage.getItem('seyes-choix_couleurs')            
        }
        else {            
            change_police('Belle Allure CE');
            // Couleur par défaut
            applique_couleur.style.backgroundColor='#0000FF'; // Pour le bouton principal
            choix_couleurs.value='#0000FF'; // Pour le sélecteur
            raz();
        }   
        checkUrl();

    }
}

function checkUrl() {
    console.log('Lecture des paramètres de l\'URL');
    
    // Récupération de l'URL et des paramètres
    let url = window.location.search;
    let urlParams = new URLSearchParams(url);

    // Vérification des différents paramètres attendus dans l'URL
    if (urlParams.has('marge')) {
        let texteMargeRecupere = urlParams.get('marge');
        console.log(texteMargeRecupere);
        document.getElementById('texte_marge').innerHTML = texteMargeRecupere;  // Met à jour la marge
    }

    if (urlParams.has('police')) {
        let police = urlParams.get('police');
        change_police(police);
    }

    if (urlParams.has('largeur_carreau')) {
        let largeurCarreau = urlParams.get('largeur_carreau');
        largeur_carreau = parseInt(largeurCarreau);  // Applique la taille de la police
        zoom(0, police_active);
    }

    // Récupération du hash pour le texte principal
    if (window.location.hash) {
        let textePrincipalRecupere = decodeURIComponent(window.location.hash.substring(1));  // Enlève le '#' et décode
        console.log(textePrincipalRecupere);
        document.getElementById('texte_principal').innerHTML = textePrincipalRecupere;  // Met à jour le texte principal
    }

    // Nettoyage de l'URL après traitement des paramètres
    const baseUrl = window.location.protocol + "//" + window.location.host + window.location.pathname;
    
    // Remplace l'URL actuelle avec l'URL de base (sans les paramètres et sans hash) sans recharger la page
    window.history.replaceState({}, document.title, baseUrl);
    
    console.log('URL nettoyée : ' + baseUrl);
}




function majLien() {
    // Base URL (tu peux la modifier selon tes besoins)
    const baseURL = window.location.protocol + "//" + window.location.host + window.location.pathname + '?';

    // Construction des paramètres de l'URL en encodant les contenus des textes et les variables globales
    const params = new URLSearchParams({
        marge: texte_marge.innerHTML,
        police: police_active,
        largeur_carreau: largeur_carreau
    });

    // Encodage du texte principal dans le hash
    const hash = encodeURIComponent(texte_principal.innerHTML);

    // Construction du lien final avec le hash pour le texte principal
    const lienFinal = baseURL + params.toString() + '#' + hash;

    console.log(lienFinal);
    zone_lien.innerHTML = lienFinal;
}


// Fonction pour copier le lien et afficher un popup
function copier() {
    // Récupération du contenu de la zone de lien
    const zoneLien = document.getElementById('zone_lien');
    const texteLien = zoneLien.innerHTML;

    // Copie du texte dans le presse-papiers
    navigator.clipboard.writeText(texteLien)
        .then(() => {
            // Création du popup "Lien copié"
            const popup = document.createElement('div');
            popup.innerHTML = "Lien copié";
            popup.style.position = "absolute";
            popup.style.backgroundColor = "black";
            popup.style.color = "white";
            popup.style.fontSize = "12px";
            popup.style.padding = "5px 10px";
            popup.style.borderRadius = "5px";
            popup.style.zIndex = "1000";
            popup.style.opacity = "0";
            popup.style.transition = "opacity 0.3s";

            // Position du popup près du bouton "copier"
            const bouton = document.getElementById('copier');
            const rect = bouton.getBoundingClientRect();
            popup.style.left = rect.left + window.scrollX + "px";
            popup.style.top = rect.top + window.scrollY - 30 + "px";  // 30px au-dessus du bouton

            // Ajout du popup au document
            document.body.appendChild(popup);

            // Faire apparaître le popup
            setTimeout(() => {
                popup.style.opacity = "1";
            }, 10);

            // Faire disparaître le popup après 2 secondes
            setTimeout(() => {
                popup.style.opacity = "0";
                setTimeout(() => {
                    document.body.removeChild(popup);
                }, 300);  // Retire le popup du DOM après la transition
            }, 2000);
        })
        .catch((err) => {
            console.error('Erreur lors de la copie : ', err);
        });
}

function insereDate(){
    // Insertion de la date du jour
    let date_du_jour=date();
    let date_formatee=date_du_jour.charAt(0).toUpperCase() + date_du_jour.slice(1);
    return date_formatee;
}

lire();
// Placement des outils
boutons.style.bottom='0px';
boutons.style.left=page.offsetLeft + page.offsetWidth/2 - boutons.offsetWidth/2 + 'px';
// Cocher la bonne option dans le menu polices
boutons_police.elements['police'][polices.indexOf(police_active)].checked=true;

// Si OpenBoard
if (window.widget) {
    // Quand on quitte le widget
    window.widget.onleave.connect(() => {
    window.sankore.setPreference('police',police_active);
    window.sankore.setPreference('texte_principal',texte_principal.innerHTML);
    window.sankore.setPreference('texte_marge',texte_marge.innerHTML);
    window.sankore.setPreference('applique_couleur',applique_couleur.style.backgroundColor);
    window.sankore.setPreference('choix_couleurs',choix_couleurs.value);
    });
}

function zoom(coef,police){
    largeur_carreau=largeur_carreau+coef;
    taille_police=largeur_carreau*facteur_taille_police;
    texte_principal.style.fontWeight=epaisseur_police;
    texte_marge.style.fontWeight=epaisseur_police;
    if (isWindows){calc_marge=largeur_carreau*coef_marge_windows}
    else {calc_marge=largeur_carreau*coef_marge}
    if (police==='Open Dyslexic'){calc_marge=calc_marge*0.90}
    page.style.backgroundSize=largeur_carreau+'px';
    body.style.backgroundSize=largeur_carreau+'px';
    texte_principal.style.fontSize=taille_police+'px';
    texte_principal.style.marginTop=calc_marge+'px';
    texte_principal.style.marginLeft=largeur_carreau/5+'px';
    texte_principal.style.marginRight=largeur_carreau/5+'px';
    texte_principal.style.lineHeight=largeur_carreau+'px';
    texte_marge.style.fontSize=taille_police+'px';
    texte_marge.style.marginTop=calc_marge+'px'
    texte_marge.style.marginLeft=largeur_carreau/5+'px';
    texte_marge.style.marginRight=largeur_carreau/5+'px';
    texte_marge.style.lineHeight=largeur_carreau+'px';
}

function pleinecran(){
    if (pleinecran_on){
        fullscreen.style.backgroundPosition=null;
        pleinecran_sortir();
    } else {
        fullscreen.style.backgroundPosition='0% 0%';
        pleinecran_ouvrir();
    }
    pleinecran_on=!pleinecran_on;
}

function pleinecran_ouvrir(){   
    // Demande le mode plein écran pour l'élément
    if (site.requestFullscreen) {
        site.requestFullscreen();        
    } else if (site.mozRequestFullScreen) { // Pour Firefox
        site.mozRequestFullScreen();
    } else if (site.webkitRequestFullscreen) { // Pour Chrome, Safari et Opera
        site.webkitRequestFullscreen();
    } else if (site.msRequestFullscreen) { // Pour Internet Explorer et Edge
        site.msRequestFullscreen();
    }
}

function pleinecran_sortir(){
    // Pour sortir du mode plein écran
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.mozCancelFullScreen) { // Pour Firefox
        document.mozCancelFullScreen();        
    } else if (document.webkitExitFullscreen) { // Pour Chrome, Safari et Opera
        document.webkitExitFullscreen();
    } else if (document.msExitFullscreen) { // Pour Internet Explorer et Edge
        document.msExitFullscreen();
    }
    recupere_boutons();
}

function nouveauTexte(){
    confirmationNouveau.style.display='block';
}

function ferme_confirmation(){
    confirmationNouveau.style.display='none';
}

function raz(){
    texte_marge.innerHTML='';
    texte_principal.innerHTML=insereDate();
    save_param();
}

function recupere_boutons(){
    setTimeout(function() {
        const positionBoutons=boutons.getBoundingClientRect();
        const largeurFenetre = window.innerWidth;
        const hauteurFenetre = window.innerHeight;
        if (positionBoutons.x>largeurFenetre){boutons.style.left=largeurFenetre-positionBoutons.width+'px';}
        if (positionBoutons.y>hauteurFenetre){boutons.style.top=hauteurFenetre-positionBoutons.height+'px';}
    }, 300);

}


function souligne(couleur) {

    if (couleur){
        couleurSoulignementActive=couleur;
        let boutonClique=document.getElementById('souligne_'+couleur);
        document.getElementById('souligne').style.backgroundImage=boutonClique.style.backgroundImage;
    } else {
        couleur=couleurSoulignementActive;
    }

    const selection = window.getSelection();
    if (!selection.rangeCount) return;

    const range = selection.getRangeAt(0);
    const selectedContent = range.extractContents();
    const fragment = document.createDocumentFragment();

    selectedContent.childNodes.forEach(node => {
        fragment.appendChild(processNode(node, couleur));
    });

    // Supprimer la sélection et insérer le contenu mis à jour
    range.deleteContents();
    range.insertNode(fragment);

    // Fusionner les spans adjacents après insertion
    mergeAdjacentSpans(range.commonAncestorContainer);

    // Désactiver la sélection
    selection.removeAllRanges();
}

function processNode(node, couleur) {
    if (node.nodeType === Node.TEXT_NODE) {
        // Le texte brut est encapsulé dans un span s'il doit être souligné
        return processTextNode(node, couleur);
    } else if (node.nodeType === Node.ELEMENT_NODE && node.tagName === "SPAN") {
        // Si c'est un span, on vérifie s'il faut le modifier
        return processSpanNode(node, couleur);
    } else {
        return node; // Pour d'autres types de nœuds, on les retourne tels quels
    }
}

function processTextNode(node, couleur) {
    // Appliquer un soulignement au texte brut, sauf si "vide"
    if (couleur === "vide") {
        return document.createTextNode(node.textContent); // Pas de balise, juste du texte brut
    } else {
        const span = document.createElement('span');
        span.classList.add('souligne', couleur);
        span.textContent = node.textContent;
        return span;
    }
}

function processSpanNode(node, couleur) {
    // Si le texte est déjà dans un span, on ajuste les classes
    if (couleur === "vide") {
        // Si on veut supprimer le soulignement, on renvoie le texte brut
        return document.createTextNode(node.textContent);
    } else if (node.classList.contains('souligne') && node.classList.contains(couleur)) {
        // Si le span a déjà la bonne classe, on le garde tel quel
        return node;
    } else {
        // Sinon, on remplace les classes existantes par la nouvelle couleur
        node.classList.remove(...node.classList);
        node.classList.add('souligne', couleur);
        return node;
    }
}

function mergeAdjacentSpans(container) {
    // Fusionne les spans adjacents avec les mêmes classes
    let node = container.firstChild;
    while (node) {
        if (node.nodeType === Node.ELEMENT_NODE && node.tagName === "SPAN") {
            let nextNode = node.nextSibling;
            while (nextNode && nextNode.nodeType === Node.ELEMENT_NODE && nextNode.tagName === "SPAN") {
                if (areSpansMergeable(node, nextNode)) {
                    // Fusionner les deux spans
                    node.textContent += nextNode.textContent;
                    nextNode.remove();
                } else {
                    break;
                }
                nextNode = node.nextSibling;
            }
        }
        node = node.nextSibling;
    }
}

function areSpansMergeable(span1, span2) {
    // Vérifie si deux spans peuvent être fusionnés en comparant leurs classes
    return span1.className === span2.className;
}

function ligatures(mode,sens_force){
    if (ligatures_on){
        if (mode!='auto'){
            bouton_ligatures.style.backgroundPosition=null;
            ligatures_on=false;
            texte_principal.spellcheck = true;
            texte_marge.spellcheck = true;
        }
        sens=[1,0];
    } else {
        if (mode!='auto'){
            bouton_ligatures.style.backgroundPosition='0% 0%';
            ligatures_on=true;
            texte_principal.spellcheck = false;
            texte_marge.spellcheck = false;
        }
        sens=[0,1];
    }
    if (sens_force){sens=sens_force}    
    let texte=texte_principal.innerHTML;
    for (let i = 0; i < subsitutions.length; i++) {
        let recherche = subsitutions[i][sens[0]];
        let subsitution = subsitutions[i][sens[1]];
        texte = texte.replace(new RegExp(recherche, 'g'), subsitution);
    }
    texte_principal.innerHTML=texte;
    texte=texte_marge.innerHTML;
    for (let i = 0; i < subsitutions.length; i++) {
        let recherche = subsitutions[i][sens[0]];
        let subsitution = subsitutions[i][sens[1]];
        texte = texte.replace(new RegExp(recherche, 'g'), subsitution);
    }
    texte_marge.innerHTML=texte;
}


function ouvreBoutonsSouligne(event) {
    event.stopPropagation();
    document.getElementById('boutons_soulignement').classList.toggle('hide');
}


function update_souligne() {
    texte_principal.style.textUnderlineOffset=distance_soulignage+'em';
    texte_marge.style.textUnderlineOffset=distance_soulignage+'em';
}

function couleurs(couleur) {
    if (focus_texte){focus_texte.focus();}
    applique_couleur.style.backgroundColor=couleur;
    document.execCommand('foreColor', false, couleur);
        // Récupérer la sélection actuelle
        var selection = window.getSelection();
        if (selection.rangeCount > 0) {
            var range = selection.getRangeAt(0);
        }
}


//Opacité
function opacite(valeur){
    voile_page.style.backgroundColor='rgba(255,255,255,'+(+1-valeur)+')';
    voile_marge.style.backgroundColor='rgba(255,255,255,'+(+1-valeur)+')';
}


// Fonction pour positionner le curseur
function positionCursor(position) {
    const range = document.createRange();
    const select = window.getSelection();
    console.log("function positioncursor "+position)
    range.setStart(texte_principal.firstChild, position);
    range.collapse(true);

    select.removeAllRanges();
    select.addRange(range);

    texte_principal.focus();
}

function clic(event){

    if (!event.target.classList.contains('deroule_souligne')) {
        console.log('Fermeture auto')
        document.getElementById('boutons_soulignement').classList.add('hide');
    }

    const borderWidth = 10; // Largeur de la bordure (en pixels)
    const rect = page.getBoundingClientRect(); // Récupère les dimensions et la position de la div "page"
    posX = event?.targetTouches?.[0]?.clientX || event.clientX;
    posY = event?.targetTouches?.[0]?.clientY || event.clientY;
    const mouseX =  posX - rect.left; // Position de la souris horizontalement par rapport à la div "page"

   
    let draggable_item=false;
    if (event.target.classList.contains('draggable_item')){draggable_item=true;}

    if (event.target.classList.contains('draggable') || draggable_item) {
        event.preventDefault();
        if (draggable_item){dragged=zone_menu_police}
        else {dragged = event.target;}
        diffsourisx = posX - dragged.offsetLeft;
        diffsourisy = posY - dragged.offsetTop;
        posX_objet = dragged.offsetLeft;
        posY_objet = dragged.offsetTop;
        dragged.classList.add('dragged');    
    } else if (mouseX <= borderWidth && mouseX>-borderWidth) { // Vérifie si le clic est dans une zone de 5px de la bordure gauche
        redim=true;
        posXDepartSouris=posX;
        largeur_page=page.offsetWidth;
        largeur_marge=marge.offsetWidth;
    }
}


function move(event) {
    posXSouris=event?.targetTouches?.[0]?.clientX || event?.clientX;
    if (redim){
        diffX = posXSouris - posXDepartSouris;
        if (largeur_marge + diffX >=60){
            page.style.width=largeur_page - diffX - 3 + 'px';
            marge.style.width=largeur_marge + diffX + 'px';
        } else {
            page.style.width='calc (100% - 63px)';
            marge.style.width='60 px';
        }
    }

    if (dragged) {
        dragged.style.right=null;
        dragged.style.bottom=null;
        event.preventDefault();
        const pageX = event?.targetTouches?.[0]?.clientX || event?.clientX || 0;
        const pageY = event?.targetTouches?.[0]?.clientY || event?.clientY || 0;
        dragged.style.left = pageX - diffsourisx + "px";
        dragged.style.top = pageY - diffsourisy + "px";
    }
}


function release(event) {
    redim=false;
      /* Si une image est en cours de déplacement */
    if (dragged) {
        // Réactiver le défilement
        dragged.classList.remove('dragged');
        dragged = null;
    }
}


function a_propos (){
    if (a_propos_on){
        a_propos_on=false;
        texte_principal.contentEditable = 'true';
        texte_principal.innerHTML=sauvegarde;
        bouton_a_propos.style.backgroundPosition=null;

    } else {
        a_propos_on=true;
        texte_principal.contentEditable = 'false';
        sauvegarde=texte_principal.innerHTML;
        texte_principal.innerHTML=texte_apropos.innerHTML;
        if (police_active!='AA Cursive'){
            ligatures('auto',[1,0])
        }
        bouton_a_propos.style.backgroundPosition='0% 0%';
    }
}


function menu_police() {
    if (zone_menu_police.style.display==='block'){ferme_menu_police()}
    else {zone_menu_police.style.display='block';}
}

function ferme_menu_police() {
    zone_menu_police.style.display='none';
}

function menu_copier_lien() {
    majLien();
    if (zone_menu_copie_lien.style.display==='block'){ferme_menu_copie_lien()}
    else {zone_menu_copie_lien.style.display='block';}
}

function ferme_menu_copie_lien() {
    zone_menu_copie_lien.style.display='none';
}


function change_police(police) {
    texte_principal.style.fontFamily=police;
    texte_marge.style.fontFamily=police;
    police_active=police;

    if (!window.widget){
        localStorage.setItem('seyes-police',police);
    }

    if (police==='AA Cursive'){
        facteur_taille_police=0.75;
        epaisseur_police='normal';
        coef_marge=0.35;
        coef_marge_windows=0.3125;
        distance_soulignage=0.3;
        bouton_ligatures.style.display=null;
        if (ligatures_on){ligatures('auto',[0,1])}
    }
    else {
        bouton_ligatures.style.display='none';
        ligatures('auto',[1,0]);
        if (police==='Open Dyslexic'){
            facteur_taille_police=0.45;
            epaisseur_police='normal';
            coef_marge=0.35;
            coef_marge_windows=0.35;
            distance_soulignage=0.5;
        }
        if (police==='Belle Allure CE' || police==='Belle Allure CM' || police==='Belle Allure GS'){
            facteur_taille_police=0.6;
            epaisseur_police='bold';
            coef_marge=0.375;
            coef_marge_windows=0.334821429;
            distance_soulignage=0.38;
        }
        if (police==='Acceseditionscursive'){
            facteur_taille_police=0.674;
            epaisseur_police='normal';
            coef_marge=0.272;
            coef_marge_windows=0.242857143;
            distance_soulignage=0.379;
        }
    }
    zoom(0,police);
    update_souligne();
}

function cree_image(event){
    let input=event.target;
    let nouvelle_image=new Image();

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            nouvelle_image.src = e.target.result;
        };

        reader.readAsDataURL(input.files[0]);
    }

    nouvelle_image.style.position='absolute';
    nouvelle_image.style.top='100px';
    nouvelle_image.style.left='100px';
    nouvelle_image.classList.add('draggable');
    nouvelle_image.style.width='300px';

    body.appendChild(nouvelle_image);
}


function date() {
    let options = {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric'
      };
    let dateDuJour = new Date().toLocaleDateString('fr-FR',options); // Obtient la date actuelle au format français (jour/mois/année)
    return dateDuJour
}

texte_principal.addEventListener("focusout", save_param);
texte_marge.addEventListener("focusout", save_param);

// Écouteurs de souris
document.addEventListener("touchstart", clic);
document.addEventListener("touchmove", move);
document.addEventListener("touchend", release);
// Pour le tactile
document.addEventListener("mousedown", clic);
document.addEventListener("mousemove", move);
document.addEventListener("mouseup", release);

//Raccourcis clavier





function save_param() {
    if (!window.widget) {
        console.log("save "+texte_marge.innerHTML)
        localStorage.setItem('seyes-police',police_active);
        localStorage.setItem('seyes-texte_principal',texte_principal.innerHTML);
        localStorage.setItem('seyes-texte_marge',texte_marge.innerHTML);
        localStorage.setItem('seyes-applique_couleur', applique_couleur.style.backgroundColor);
        localStorage.setItem('seyes-choix_couleurs', choix_couleurs.value);
    }
}



function ramenerFocus(div) {
    div.focus();
}

texte_principal.addEventListener('blur', function() {
    focus_texte=texte_principal;
});

texte_marge.addEventListener('blur', function() {
    focus_texte=texte_marge;
});


page.addEventListener('paste', function(e) {    
    // Quand un "coller" est détecté

    e.preventDefault(); // On inhibe le comportement "normal".

    //On récupère le contenu du presse-papiers
    var text = (e.originalEvent || e).clipboardData.getData('text/plain');

    // On supprime le formatage HTML.
    text = text.replace(/<[^>]*>/g, '');
    
    // On insère le texte sans styles.
    document.execCommand('insertText', false, text);
});


function ajouterBackgroundImages() {
    console.log('ajout des images');
    // Sélectionner tous les boutons dans la div boutons_soulignement
    const boutons = document.querySelectorAll('#boutons_soulignement button');

    // Parcourir chaque bouton
    boutons.forEach(bouton => {
        // Récupérer l'ID du bouton
        const boutonId = bouton.id;

        // Construire l'URL de l'image en fonction de l'ID du bouton
        const imageUrl = `images/${boutonId}.svg`;
        console.log(imageUrl);

        // Appliquer le background-image au bouton
        bouton.style.backgroundImage = `url('${imageUrl}')`;

    });
}

// Appeler la fonction pour appliquer les images après le chargement du DOM
window.onload = ajouterBackgroundImages;