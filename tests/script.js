// Seyes est un logiciel libre
// sous licence GNU GPL
/// écrit par Arnaud Champollion


//Éléments DOM
const marge=document.getElementById('marge');
const page=document.getElementById('page');
const fullscreen=document.getElementById('fullscreen');
const bouton_a_propos=document.getElementById('a_propos');
const bouton_ligatures=document.getElementById('bouton_ligatures');
const texte_principal=document.getElementById('texte_principal');
const body = document.body;
const site = document.documentElement;
const texte_apropos = document.getElementById('texte_apropos');
const boutons = document.getElementById('boutons');
const texte_marge = document.getElementById('texte_marge');
const choix_couleurs = document.getElementById('choix_couleurs');
const zone_menu_police = document.getElementById('zone_menu_police');
const boutons_police = document.getElementById('boutons_police');
const applique_couleur = document.getElementById('applique_couleur');
const input_taille_police = document.getElementById('taille_police');
const input_marge = document.getElementById('marge_haut');
const input_marge_souligne = document.getElementById('marge_souligne');





// Autres constantes
const isWindows = /Win/i.test(navigator.userAgent);
console.log("Sommes-nous sur Windows ? "+isWindows)

// Pour les ligatures
let signes=['═','║']
let subsitutions=[
    ['on','═'],
    ['or','║'],
    ['s ','╝ '],
    ['s\\.','╝.'],
    ['s,','╝,'],
    ['s;','╝;'],
    ['s:','╝:'],
    ['s!','╝!'],
    ['s/','╝/'],
    ['s-','╝-'],
    ["s'","╝'"],
    [' p',' ╰'],
    [' j',' ╮'],
    [' s',' ╠'],
    [' i',' ╱']
]

let polices=['AA Cursive','Open Dyslexic','Belle Allure CE']

// Variables globales
largeur_carreau=80;
facteur_taille_police=0.75
taille_police=60;
epaisseur_police='normal';
pleinecran_on=false;
a_propos_on=false;
redim=false;
ligatures_on=false;
dragged=null;
coef_marge=0.35;
coef_marge_windows=0.3125;
distance_soulignage=0.3;
focus_texte=null;

// Commandes à lancer au départ
// Récupération du réglage de police dans la mémoire locale
if (localStorage.getItem('police')){change_police(localStorage.getItem('police'))}
else {change_police('AA Cursive')}
// Couleur par défaut
applique_couleur.style.backgroundColor='#0000FF'; // Pour le bouton principal
choix_couleurs.value='#0000FF'; // Pour le sélecteur
// Insertion de la date du jour
let date_du_jour=date()
texte_principal.innerHTML=date_du_jour.charAt(0).toUpperCase() + date_du_jour.slice(1);
// Placement des outils
boutons.style.bottom='0px';
boutons.style.left=page.offsetLeft + page.offsetWidth/2 - boutons.offsetWidth/2 + 'px';
// Cocher la bonne option dans le menu polices
boutons_police.elements['police'][polices.indexOf(police_active)].checked=true;

function maj_input(){
    input_taille_police.value=facteur_taille_police;
    input_marge.value=coef_marge;
    input_marge_souligne.value=distance_soulignage;
}

function maj_reglages(){
    facteur_taille_police=input_taille_police.value;
    console.log(facteur_taille_police)
    coef_marge_windows=input_marge.value;
    coef_marge=input_marge.value;
    distance_soulignage=input_marge_souligne.value;
    zoom(0);
    update_souligne();
}

maj_input();

function zoom(coef,police){
    largeur_carreau=largeur_carreau+coef;
    taille_police=largeur_carreau*facteur_taille_police;
    texte_principal.style.fontWeight=epaisseur_police;
    texte_marge.style.fontWeight=epaisseur_police;
    if (isWindows){calc_marge=largeur_carreau*coef_marge_windows}
    else {calc_marge=largeur_carreau*coef_marge}
    if (police==='Open Dyslexic'){calc_marge=calc_marge*0.90}
    page.style.backgroundSize=largeur_carreau+'px';
    body.style.backgroundSize=largeur_carreau+'px';
    texte_principal.style.fontSize=taille_police+'px';
    texte_principal.style.marginTop=calc_marge+'px';
    texte_principal.style.marginLeft=largeur_carreau/5+'px';
    texte_principal.style.marginRight=largeur_carreau/5+'px';
    texte_principal.style.lineHeight=largeur_carreau+'px';
    texte_marge.style.fontSize=taille_police+'px';
    texte_marge.style.marginTop=calc_marge+'px'
    texte_marge.style.marginLeft=largeur_carreau/5+'px';
    texte_marge.style.marginRight=largeur_carreau/5+'px';
    texte_marge.style.lineHeight=largeur_carreau+'px';
}

function pleinecran(){
    if (pleinecran_on){
        fullscreen.style.backgroundPosition=null;
        pleinecran_sortir();
    } else {
        fullscreen.style.backgroundPosition='0% 0%';
        pleinecran_ouvrir();
    }
    pleinecran_on=!pleinecran_on;
}

function pleinecran_ouvrir(){   
    // Demande le mode plein écran pour l'élément
    if (site.requestFullscreen) {
        site.requestFullscreen();        
    } else if (site.mozRequestFullScreen) { // Pour Firefox
        site.mozRequestFullScreen();
    } else if (site.webkitRequestFullscreen) { // Pour Chrome, Safari et Opera
        site.webkitRequestFullscreen();
    } else if (site.msRequestFullscreen) { // Pour Internet Explorer et Edge
        site.msRequestFullscreen();
    }
}

function pleinecran_sortir(){
    // Pour sortir du mode plein écran
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.mozCancelFullScreen) { // Pour Firefox
        document.mozCancelFullScreen();        
    } else if (document.webkitExitFullscreen) { // Pour Chrome, Safari et Opera
        document.webkitExitFullscreen();
    } else if (document.msExitFullscreen) { // Pour Internet Explorer et Edge
        document.msExitFullscreen();
    }
    recupere_boutons();
}

function recupere_boutons(){
    setTimeout(function() {
        const positionBoutons=boutons.getBoundingClientRect();
        const largeurFenetre = window.innerWidth;
        const hauteurFenetre = window.innerHeight;
        if (positionBoutons.x>largeurFenetre){boutons.style.left=largeurFenetre-positionBoutons.width+'px';}
        if (positionBoutons.y>hauteurFenetre){boutons.style.top=hauteurFenetre-positionBoutons.height+'px';}
    }, 300);

}

function souligne(){
    no_souligne();
    
    const selection = window.getSelection(); // Obtient la sélection

    // Vérifie si la sélection est non vide et dans la div éditable
    if (selection.toString() && texte_principal.contains(selection.anchorNode)) {
      // Crée un span pour entourer le texte sélectionné
      const span = document.createElement('span');
      span.classList.add('souligne');
  
      // Remplace le texte sélectionné par le span avec le style appliqué
      const range = selection.getRangeAt(0).cloneRange();
      range.surroundContents(span);
      selection.removeAllRanges();
      selection.addRange(range);
    }


}

function no_souligne(){    
    // Sélectionne tous les éléments <span> dans le document
    const spans = texte_principal.querySelectorAll('span.souligne');

    // Parcours les éléments <span> avec la classe 'souligne'
    for (let i = 0; i < spans.length - 1; i++) {
        const currentSpan = spans[i];
        const nextSpan = spans[i + 1];

        // Vérifie si les éléments suivants sont des balises <span> avec la classe 'souligne'
        if (nextSpan && nextSpan.previousElementSibling === currentSpan.nextElementSibling) {
            // Fusionne le contenu des balises <span>
            currentSpan.textContent += nextSpan.textContent;

            // Supprime le prochain <span> après fusion
            nextSpan.parentNode.removeChild(nextSpan);
        }
    }
}



function ligatures(mode,sens_force){
    if (ligatures_on){
        if (mode!='auto'){
            bouton_ligatures.style.backgroundPosition=null;
            ligatures_on=false;
            texte_principal.spellcheck = true;
            texte_marge.spellcheck = true;
        }
        sens=[1,0];
    } else {
        if (mode!='auto'){
            bouton_ligatures.style.backgroundPosition='0% 0%';
            ligatures_on=true;
            texte_principal.spellcheck = false;
            texte_marge.spellcheck = false;
        }
        sens=[0,1];
    }
    if (sens_force){sens=sens_force}    
    let texte=texte_principal.innerHTML;
    for (let i = 0; i < subsitutions.length; i++) {
        let recherche = subsitutions[i][sens[0]];
        let subsitution = subsitutions[i][sens[1]];
        texte = texte.replace(new RegExp(recherche, 'g'), subsitution);
    }
    texte_principal.innerHTML=texte;
    texte=texte_marge.innerHTML;
    for (let i = 0; i < subsitutions.length; i++) {
        let recherche = subsitutions[i][sens[0]];
        let subsitution = subsitutions[i][sens[1]];
        texte = texte.replace(new RegExp(recherche, 'g'), subsitution);
    }
    texte_marge.innerHTML=texte;
}

function souligner() {
    document.execCommand('underline', false, null);
    update_souligne();
}

function nePasSouligner() {
    document.execCommand('underline', false, null);
}

function update_souligne() {
    texte_principal.style.textUnderlineOffset=distance_soulignage+'em';
    texte_marge.style.textUnderlineOffset=distance_soulignage+'em';
}

function couleurs(couleur) {
    if (focus_texte){focus_texte.focus();}
    applique_couleur.style.backgroundColor=couleur;
    document.execCommand('foreColor', false, couleur);
        // Récupérer la sélection actuelle
        var selection = window.getSelection();
        if (selection.rangeCount > 0) {
            var range = selection.getRangeAt(0);
    
            // Créer un élément <font> avec les attributs nécessaires
            var fontElement = document.createElement('font');
            fontElement.setAttribute('color', color);
    
            // Insérer l'élément <font> vide à la position du curseur
            range.deleteContents(); // Supprimer le contenu actuel de la sélection
            range.insertNode(fontElement);
    
            // Mettre à jour la sélection pour inclure l'élément <font>
            selection.removeAllRanges();
            selection.addRange(range);
        }
}

// Fonction pour positionner le curseur
function positionCursor(position) {
    const range = document.createRange();
    const select = window.getSelection();
    console.log("function positioncursor "+position)
    range.setStart(texte_principal.firstChild, position);
    range.collapse(true);

    select.removeAllRanges();
    select.addRange(range);

    texte_principal.focus();
}

function clic(event){
    const borderWidth = 10; // Largeur de la bordure (en pixels)
    const rect = page.getBoundingClientRect(); // Récupère les dimensions et la position de la div "page"
    posX = event?.targetTouches?.[0]?.clientX || event.clientX;
    posY = event?.targetTouches?.[0]?.clientY || event.clientY;
    const mouseX =  posX - rect.left; // Position de la souris horizontalement par rapport à la div "page"

   
    let draggable_item=false;
    if (event.target.classList.contains('draggable_item')){draggable_item=true;}

    if (event.target.classList.contains('draggable') || draggable_item) {
        event.preventDefault();
        if (draggable_item){dragged=event.target.parentNode}
        else {dragged = event.target;}
        diffsourisx = posX - dragged.offsetLeft;
        diffsourisy = posY - dragged.offsetTop;
        posX_objet = dragged.offsetLeft;
        posY_objet = dragged.offsetTop;
        dragged.classList.add('dragged');    
    } else if (mouseX <= borderWidth && mouseX>-borderWidth) { // Vérifie si le clic est dans une zone de 5px de la bordure gauche
        redim=true;
        posXDepartSouris=posX;
        largeur_page=page.offsetWidth;
        largeur_marge=marge.offsetWidth;
    }
}


function move(event) {
    posXSouris=event?.targetTouches?.[0]?.clientX || event?.clientX;
    if (redim){
        diffX = posXSouris - posXDepartSouris;
        if (largeur_marge + diffX >=60){
            page.style.width=largeur_page - diffX - 3 + 'px';
            marge.style.width=largeur_marge + diffX + 'px';
        } else {
            page.style.width='calc (100% - 63px)';
            marge.style.width='60 px';
        }
    }

    if (dragged) {
        dragged.style.right=null;
        dragged.style.bottom=null;
        event.preventDefault();
        const pageX = event?.targetTouches?.[0]?.clientX || event?.clientX || 0;
        const pageY = event?.targetTouches?.[0]?.clientY || event?.clientY || 0;
        dragged.style.left = pageX - diffsourisx + "px";
        dragged.style.top = pageY - diffsourisy + "px";
    }
}


function release(event) {
    redim=false;
      /* Si une image est en cours de déplacement */
    if (dragged) {
        // Réactiver le défilement
        dragged.classList.remove('dragged');
        dragged = null;
    }
}


function a_propos (){
    if (a_propos_on){
        a_propos_on=false;
        texte_principal.contentEditable = 'true';
        texte_principal.innerHTML=sauvegarde;
        bouton_a_propos.style.backgroundPosition=null;

    } else {
        a_propos_on=true;
        texte_principal.contentEditable = 'false';
        sauvegarde=texte_principal.innerHTML;
        texte_principal.innerHTML=texte_apropos.innerHTML;
        if (police_active!='AA Cursive'){
            ligatures('auto',[1,0])
        }
        bouton_a_propos.style.backgroundPosition='0% 0%';
    }
}


function menu_police() {
    zone_menu_police.style.display='block';
}

function ferme_menu_police() {
    zone_menu_police.style.display='none';
}

function change_police(police) {
    texte_principal.style.fontFamily=police;
    texte_marge.style.fontFamily=police;
    police_active=police;

    localStorage.setItem('police',police);

    if (police==='AA Cursive'){
        facteur_taille_police=0.75;
        epaisseur_police='normal';
        coef_marge=0.35;
        coef_marge_windows=0.3125;
        distance_soulignage=0.3;
        bouton_ligatures.style.display=null;
        if (ligatures_on){ligatures('auto',[0,1])}
    }
    else {
        bouton_ligatures.style.display='none';
        ligatures('auto',[1,0]);
        if (police==='Open Dyslexic'){
            facteur_taille_police=0.45;
            epaisseur_police='normal';
            coef_marge=0.35;
            coef_marge_windows=0.35;
            distance_soulignage=0.5;
        }
        if (police==='Belle Allure CE'){
            facteur_taille_police=0.6;
            epaisseur_police='bold';
            coef_marge=0.375;
            coef_marge_windows=0.334821429;
            distance_soulignage=0.38;
        }
        if (police==='Acceseditionscursive'){
            facteur_taille_police=0.674;
            epaisseur_police='normal';
            coef_marge=0.272;
            coef_marge_windows=0.242857143;
            distance_soulignage=0.379;
        }
    }
    zoom(0,police);
    update_souligne();
    maj_input();
}

function cree_image(event){
    let input=event.target;
    let nouvelle_image=new Image();

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            nouvelle_image.src = e.target.result;
        };

        reader.readAsDataURL(input.files[0]);
    }

    nouvelle_image.style.position='absolute';
    nouvelle_image.style.top='100px';
    nouvelle_image.style.left='100px';
    nouvelle_image.classList.add('draggable');
    nouvelle_image.style.width='300px';    
    body.appendChild(nouvelle_image);

    let suppr=document.createElement('button');
    suppr.type='button';
    suppr.classList.add('bouton_suppr');
    nouvelle_image.appendChild(suppr);
}


function date() {
    let options = {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric'
      };
    let dateDuJour = new Date().toLocaleDateString('fr-FR',options); // Obtient la date actuelle au format français (jour/mois/année)
    return dateDuJour
}


// Écouteurs de souris
document.addEventListener("touchstart", clic);
document.addEventListener("touchmove", move);
document.addEventListener("touchend", release);
// Pour le tactile
document.addEventListener("mousedown", clic);
document.addEventListener("mousemove", move);
document.addEventListener("mouseup", release);

//Raccourcis clavier



function ramenerFocus(div) {
    div.focus();
}

texte_principal.addEventListener('blur', function() {
    focus_texte=texte_principal;
});

texte_marge.addEventListener('blur', function() {
    focus_texte=texte_marge;
});
